+++
title = "Administration scripts"
draft = false
+++

Here is the list of administration scripts which are included in SystemRescue:

* [build-zfs-srm](/scripts/build-zfs-srm/) Build a SystemRescueModule (SRM) that enables ZFS support
* [mountall](/scripts/mountall/) Mount all disks and volumes
* [reverse_ssh](/scripts/reverse_ssh/) helps you get an SSH access to a computer
  running SystemRescue which is located behind a NAT router or firewall.
* [sysrescue-customize](/scripts/sysrescue-customize/) customize SystemRescue ISO-images 
  and add your own files
